const path = require('path');

var config =  {
  entry: './src/index.ts',
  devServer: {
      contentBase: './public'
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ]
  },
  output: {
    filename: '[name].browser.js',
    path: path.resolve(__dirname, '../public')
  }
};

module.exports = (env, argv) => {
    if(argv.mode === 'development'){
       config.devtool= 'inline-source-map';
       config.mode = 'development'
    }
    if(argv.mode === 'production'){
        config.mode = 'production'
    }
    return config
}