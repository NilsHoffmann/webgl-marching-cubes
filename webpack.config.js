"use strict";

let browserConfig = require("./config/browser.webpack.config.js");
let workerConfig = require("./config/worker.webpack.config.js");

module.exports = [
    browserConfig,
    workerConfig
];