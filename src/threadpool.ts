import { Vector3 } from "three"

class BlockingQueue<T, R> {
    available: Array<T>
    waiting: Array<{ task: (t: T) => any, resolve: (value?: R | PromiseLike<R>) => void }> = []

    constructor(available: Array<T>) {
        this.available = available
    }
    public submit(task: (w: T) => R): Promise<R> {
        return new Promise(async (resolve, reject) => {
            if (this.available.length > 0) {
                let ressource = this.available.pop()
                let promise = task(ressource)
                await promise
                resolve(promise)

                this.addAvailable(ressource)
            }
            else {
                this.waiting.push({ resolve: resolve, task: task })
            }
        })
    }
    public async addAvailable(ressource: T) {
        this.available.push(ressource)
        if (this.waiting.length > 0) {
            
            let task = this.waiting.pop()
            let ressource = this.available.pop()
            let promise = task.task(ressource)
            await promise
            
            task.resolve(promise)

            this.addAvailable(ressource)
        }
    }

}

export class Threadpool {


    threadcount: number;
    workers: BlockingQueue<Worker, Promise<{ positions: number[], colors: number[] } | Error>>
    constructor(threads: number) {
        this.threadcount = threads;

        let workers = []
        for (let i = 0; i < this.threadcount; i++) {
            workers.push(new Worker('main.worker.js'))
        }
        this.workers = new BlockingQueue(workers)
    }




    public calcChunkOnWorker(pos: Vector3): Promise<Promise<{ positions: number[], colors: number[] } | Error>> {
        let start = new Date().getTime();
        let waitingCount = this.workers.waiting.length
        return this.workers.submit(worker => {

            let promise = new Promise((resolve, reject) => {
                worker.onmessage = msg => {
                    console.log(msg);
                    console.log(`Job spent ${new Date().getTime() - start} ms in Queue, number of waiting jobs at submit: ${waitingCount}`)
                    resolve({ positions: msg.data.positions, colors: msg.data.colors });
                };
                worker.onerror = error => {
                    console.error(error);
                    reject(error)
                }
            }) as Promise<{ positions: number[], colors: number[] } | Error>;
            worker.postMessage({ pos: pos });
            
            return promise
        })
    }

}