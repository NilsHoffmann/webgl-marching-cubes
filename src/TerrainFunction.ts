import {Vector3} from "three";

// @ts-ignore
import {Noise} from 'noisejs'

const RADIUS = 3.2;

let noise = new Noise(123)

export function samplePos(pos: Vector3) {
    let v = -pos.y + 3
    let warpedPos = new Vector3().copy(pos)
    let warp = noise.perlin3(warpedPos.x, warpedPos.y, warpedPos.z)
    warpedPos.addScalar(warp)
    v += noise.perlin3(warpedPos.x * 4.03 / 20, warpedPos.y * 4.03 / 20, warpedPos.z * 4.03 / 20) * 0.25 * 5
    v += noise.perlin3(warpedPos.x * 1.01 / 20, warpedPos.y * 1.01 / 20, warpedPos.z * 1.01 / 20) * 0.5 * 5
    v += noise.perlin3(warpedPos.x * 1.01 / 20, warpedPos.y * 1.01 / 20, warpedPos.z * 1.01 / 20) * 2 * 5

    v += noise.perlin3(pos.x / 5, pos.y / 6, pos.z / 6) * 12
    v += noise.perlin3(pos.x / 32, pos.y / 32, pos.z / 32) * 64


    return v
}

function cube(pos: Vector3): number {
    return pos.distanceTo(new Vector3(3, 3, 3)) - RADIUS

}

function clamp(v: number, lower: number = 0, upper: number = Infinity): number {
    if (v > upper) return upper
    if (v < lower) return lower
    return v
}

function weirdTerrain(pos: Vector3): number {
    let v = -pos.y + 3 //+ 15 * noise.perlin2(pos.x / 7, pos.y / 7) * noise.perlin2(pos.x / 11, pos.z / 11)
    v += noise.perlin2(pos.x / 3, pos.y / 3) * 8
    return v

}

function sinusoidal(pos: Vector3){
    let v = -pos.y + 3
    v += Math.sin(pos.x/20)
    v += Math.sin(pos.z/20)

    v += Math.sin(23/7 + pos.x/11)
    v += Math.sin(23/7 + pos.z/11)

    v += Math.floor((pos.x/67) * (pos.x/67) + (pos.z/67) * (pos.z/67))

    return v
}