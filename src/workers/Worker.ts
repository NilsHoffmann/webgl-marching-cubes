import {MarchingCubesChunk} from "../Chunk";
import {Vector3} from "three";

console.log('new worker!')
addEventListener('message', (message) =>
{
    console.log(message.data)
    if(message.data.pos === undefined){
        console.log(`got unexpected messages: ${message}`);
        return
    }
    let pos = message.data.pos as Vector3;
    let chunk = new MarchingCubesChunk(pos);
    let startTime = new Date().getTime()
    chunk.generateVertices();
    console.log(`generated the following chunk: ${pos.x}, ${pos.z}, took ${new Date().getTime() - startTime} ms`);
    let positions= chunk.positions;
    let colors = chunk.colors;
    postMessage({positions: positions, colors: colors})
});