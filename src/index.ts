import {
    AmbientLight,
    BoxGeometry,
    Color,
    DirectionalLight, DoubleSide,
    Mesh, MeshBasicMaterial,
    MeshLambertMaterial, MeshPhongMaterial,
    PerspectiveCamera, Plane, PlaneGeometry, PlaneHelper, RepeatWrapping,
    Scene, TextureLoader,
    Vector3,
    WebGLRenderer,
    HemisphereLight,
    PointLight,
    Vector2
} from 'three'
import { getMesh, MarchingCubesChunk } from "./Chunk";
import * as Stats from 'stats.js';
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { samplePos } from "./TerrainFunction";
import { Threadpool } from './threadpool';

const scene = new Scene();
let chunks: Array<Chunk> = []
let threadpool = new Threadpool(12)


const camera: PerspectiveCamera = new PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.1, 1000);


scene.add(new AmbientLight(0x444444, 0.2));
scene.add(new HemisphereLight(0xffffff, 'gray', 0.3))
let pointlight = new PointLight("yellow", 0.8)
scene.add(pointlight)

const renderer = new WebGLRenderer({ antialias: true });
let container = document.getElementById('container');
renderer.setClearColor(0xADD8E6, 1);
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.gammaInput = true;
renderer.gammaOutput = true;
window.addEventListener('resize', ev => {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight)
}, false);

container.append(renderer.domElement);
let stats = new Stats();
container.append(stats.dom);

let resetPlane = document.getElementById("reset") as HTMLButtonElement;
resetPlane.onclick = ev => plane.position.set(150, 35, 150);

camera.position.z = 10;

let textureLoader = new TextureLoader();
let waterTexture = textureLoader.load('textures/water.png');
waterTexture.wrapS = waterTexture.wrapT = RepeatWrapping;
waterTexture.repeat.set(512, 512);
let waterMaterial = new MeshPhongMaterial({
    color: 0xffffff,
    map: waterTexture,
    side: DoubleSide,
    opacity: 0.7,
    premultipliedAlpha: true,
    transparent: true
});


let waterMesh = new Mesh(new PlaneGeometry(4000, 4000), waterMaterial);
waterMesh.lookAt(new Vector3(0, 1, 0))
waterMesh.position.y = 3.1
scene.add(waterMesh);
console.log("added water");

let bumpMap = new TextureLoader().load('textures/bumpMap.jpg')


let pressed: { [key: string]: boolean } = {};

const controls = new OrbitControls(camera, renderer.domElement);
controls.enableZoom = true;


scene.background = new Color("grey");
addListeners();
let plane: Scene;
new GLTFLoader().load('scene.gltf', gltf => {
    console.log('loaded');
    gltf.scene.scale.set(1 / 25, 1 / 25, 1 / 25);
    plane = gltf.scene;
    plane.position.set(150, 35, 150);

    camera.position.x = plane.position.x;
    camera.position.y = plane.position.y + 3;
    camera.position.z = plane.position.z - 12;

    camera.lookAt(10, 10, 10)

},
    onload => console.log(onload.total),
    error => console.error(error)
);

generateNewTerrain(new Vector3(150, 35, 150))

console.log(camera.view);
animate();

function flyPlane(plane: Scene) {
    if (pressed['W']) {
        plane.rotateX(0.02)
    }
    if (pressed["S"]) {
        plane.rotateX(0 - .02)
    }
    if (pressed["A"]) {
        plane.rotateY(+0.02)
    }
    if (pressed["D"]) {
        plane.rotateY(-0.02)
    }

    if (pressed["Q"]) {
        plane.rotateZ(-0.02)
    }
    if (pressed["E"]) {
        plane.rotateZ(+0.02)
    }


    //plane always flies forwards unless in terrain
    if (samplePos(plane.position) < 0) {
        let speed = 0.1;
        let forward = new Vector3(0, 0, 1).applyQuaternion(plane.quaternion).add(new Vector3(0, -1, 0));
        speed *= forward.length();
        plane.translateZ(speed * 2)
    }

}

function animate() {
    requestAnimationFrame(animate);
    controls.update();
    stats.begin();

    let pos = new Vector3().copy(plane.position).add(new Vector3(0, 3, -12).applyQuaternion(plane.quaternion));

    camera.position.set(pos.x, pos.y, pos.z);
    pointlight.position.set(plane.position.x, plane.position.y, plane.position.z)

    camera.lookAt(plane.position);

    flyPlane(plane);
    generateNewTerrain(plane.position);

    renderer.render(new Scene().add(scene, plane), camera);
    stats.end()
}


function addListeners() {
    window.addEventListener('keydown', function (e) {
        pressed[e.key.toUpperCase()] = true;
    });
    window.addEventListener('keyup', function (e) {
        pressed[e.key.toUpperCase()] = false;
    });
}



interface Chunk {
    pos: Vector3,
    mesh?: Mesh,
    shown: boolean
}


function getChunksInRadius(pos: Vector3, radius: number): Array<Chunk> {
    let chunks: Array<Chunk> = []
    //Assumes a chunk size of 100

    let chunkCenter = new Vector3();
    for (let x = -radius; x < radius; x += 100) {
        for (let z = -radius; z < radius; z += 100) {
            chunkCenter.set(x + 50, 0, z + 50)
            let chunkCornerPos = new Vector3(Math.floor(pos.x / 100), 0, Math.floor(pos.z / 100)).multiplyScalar(100)
            if (chunkCenter.distanceTo(pos) < radius) {
                chunks.push({
                    pos: new Vector3((x > 0 ? Math.ceil(x / 100) : Math.floor(x / 100)) * 100,
                        0,
                        (z > 0 ? Math.ceil(z / 100) : Math.floor(z / 100)) * 100)
                        .add(chunkCornerPos),
                    shown: false
                })
            }
        }
    }
    return chunks
}

function generateNewTerrain(pos: Vector3) {
    let needed = getChunksInRadius(pos, 800)

    //already exsists?
    let missing: Array<Chunk> = needed.filter(need =>
        chunks.filter(existing =>
            existing.pos.distanceTo(need.pos) < 0.5).length == 0)

    missing.map(c => chunks.push(c))
    for (const chunk of missing) {
        threadpool.calcChunkOnWorker(chunk.pos).then(v => {
            let value = v as any
            if (value instanceof Error) {
                console.error(value);
                return;
            }
            chunk.mesh = getMesh(value.positions, value.colors, bumpMap)
            chunks.push(chunk)
            chunk.shown = true
            scene.add(chunk.mesh)
        }
        )
    }
}



